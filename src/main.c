#include <SDL2/SDL.h>
#include <stdlib.h>

#define WIDTH 30
#define HEIGHT 20
#define GAP 5
#define SIZE 20

int board[HEIGHT][WIDTH];
int board_copy[HEIGHT][WIDTH];

int get(int x, int y) {
    x = (WIDTH+(x%WIDTH))%WIDTH;
    y = (HEIGHT+(y%HEIGHT))%HEIGHT;
    return board[y][x];
}

int main() {
    SDL_Init(SDL_INIT_VIDEO);
    SDL_Window *app = SDL_CreateWindow("AutoCell", 
            SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
            1000, 800, SDL_WINDOW_VULKAN);
    SDL_Renderer *renderer = SDL_CreateRenderer(app, -1, 
            SDL_RENDERER_ACCELERATED);

    SDL_Event event;
    int start = 0;
    int time_elapsed = 0, pressed = 0, moved = 0;
    int lastx = -1, lasty = -1;
    int cx, cy;
    cx = cy = -1;
    while (1) {
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) {
                exit(0);
            }
            else if (event.type == SDL_MOUSEBUTTONDOWN) {
                if (!start) {
                    pressed = 1;
                    int mx = event.motion.x;
                    int my = event.motion.y;
                    cx = (mx - 100) % (SIZE+GAP) <= SIZE ? (mx - 100)/(SIZE+GAP) : -1; 
                    cy = (my - 100) % (SIZE+GAP) <= SIZE ? (my - 100)/(SIZE+GAP) : -1; 
                }
            }
            else if (event.type == SDL_MOUSEBUTTONUP) {
                int mx = event.motion.x;
                int my = event.motion.y;
                int px = (mx - 100) % (SIZE+GAP) <= SIZE ? (mx - 100)/(SIZE+GAP) : -1; 
                int py = (my - 100) % (SIZE+GAP) <= SIZE ? (my - 100)/(SIZE+GAP) : -1; 
                if (px >= 0 && px < WIDTH && py >= 0 && py < HEIGHT && !moved) {
                    board[py][px] = !board[py][px];
                }
                pressed = 0;
                moved = 0;
            }
            else if (event.type == SDL_MOUSEMOTION) {
                if (pressed) {
                    int mx = event.motion.x;
                    int my = event.motion.y;
                    int px = (mx - 100) % (SIZE+GAP) <= SIZE ? (mx - 100)/(SIZE+GAP) : -1; 
                    int py = (my - 100) % (SIZE+GAP) <= SIZE ? (my - 100)/(SIZE+GAP) : -1; 
                    if (px != cx || py != cy)
                        moved = 1;
                    if (lastx != px || lasty != py) {
                        if (px >= 0 && px < WIDTH && py >= 0 && py < HEIGHT) {
                            board[py][px] = 1;
                        }
                        lastx = px;
                        lasty = py;
                    }
                }

            }
            else if (event.type == SDL_KEYDOWN) {
                if (!start)
                    start = 1;
                else {
                    memset(board, 0, sizeof(board));
                    start = 0;
                }
            }
        }
        if (start && time_elapsed > 300) {
            memcpy(board_copy, board, sizeof(board));
            for (int i = 0; i < HEIGHT; ++i) {
                for (int j = 0; j < WIDTH; ++j) {
                    int cnt = 0;
                    for (int di = -1; di <= 1; ++di)
                        for (int dj = -1; dj <= 1; ++dj)
                            cnt += get(j+dj, i+di);
                    cnt -= get(j, i);
                    if (get(j, i) == 1 && (cnt < 2 || cnt > 3))
                        board_copy[i][j] = 0;
                    else if (get(j, i) == 0 && cnt == 3)
                        board_copy[i][j] = 1;
                }
            }
            memcpy(board, board_copy, sizeof(board));
            time_elapsed = 0;
        }
        SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
        SDL_RenderClear(renderer);

        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        SDL_Rect boundary = {100-GAP, 100-GAP, 
            (SIZE+GAP)*WIDTH+GAP, (SIZE+GAP)*HEIGHT+GAP};
        SDL_RenderDrawRect(renderer, &boundary);
        for (int i = 0; i < HEIGHT; ++i) {
            for (int j = 0; j < WIDTH; ++j) {
                SDL_Rect rect = {100+j*(SIZE+GAP), 100+i*(SIZE+GAP), SIZE, SIZE};
                if (board[i][j]) {
                    SDL_RenderFillRect(renderer, &rect);
                } else {
                    SDL_RenderDrawRect(renderer, &rect);
                }
            }
        }

        SDL_RenderPresent(renderer);

        SDL_Delay(1000/60);
        time_elapsed += 1000/60;
    }

    return 0;
}
